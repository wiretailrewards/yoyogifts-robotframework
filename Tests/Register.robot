*** Settings ***
Library   SeleniumLibrary

*** Variables ***

${BASE_URL}     https://yoyogifts-qa.wigroup.co/
${browser}      Chrome


*** Test Cases ***
Register on YoyoGifts
    open browser    https://yoyogifts-qa.wigroup.co/    ${browser}
    set browser implicit wait       5
    click element   xpath=//a[contains(@href, '/?modal=login')]
    click element   xpath=//b[contains(.,'Register Here')]
    input text      id=email   gichuhikid532@gmail.com
    input text      id=firstname    MoniQA
    input text      id=surname  Test
    input text      id=company      Nike
    input text      id=mobile       +27 76 778 0080
    input text      name=password   Admin12!@
    input text      id=confirmPassword      Admin12!@
    click element    css=.chakra-checkbox__control
    click element   xpath=//button[contains(.,'Register')]
    close browser
