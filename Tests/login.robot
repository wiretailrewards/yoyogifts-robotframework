*** Settings ***
Library   SeleniumLibrary

*** Variables ***

${BASE_URL}     https://yoyogifts-qa.wigroup.co/
${browser}      Chrome


*** Test Cases ***
Login on YoyoGifts
    LoginKW
    close browser

*** Keywords ***
LoginKW
    open browser    https://yoyogifts-qa.wigroup.co/    ${browser}
    set browser implicit wait       5
    click element   xpath=//a[contains(@href, '/?modal=login')]
    input text      id=username     monicahn@yoyowallet.com
    input text      id=password     Admin12!@
