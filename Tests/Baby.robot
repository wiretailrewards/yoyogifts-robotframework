*** Settings ***
Library   SeleniumLibrary

*** Variables ***

${BASE_URL}     https://yoyogifts-qa.wigroup.co/
${browser}      Chrome


*** Test Cases ***
Baby
    set selenium timeout    10s
    open browser    https://yoyogifts-qa.wigroup.co/    ${browser}
    maximize browser window
    reload page
    sleep   5
    click button    xpath=//button[contains(.,'Accept Cookies')]

    #get started
    scroll element into view    xpath=//a[contains(@href, '/gift/get-started/')]
    click element    xpath=//a[contains(@href, '/gift/get-started/')]
    sleep   5

    #Gift card Value
    click element   id=amount
    input text      id=amount   100
    set selenium implicit wait  5

    #select retailer
    scroll element into view    xpath=//button[12]
    click element       xpath=//button[12]
    scroll element into view        xpath=//button[contains(.,'Next')]
    click element   xpath=//button[contains(.,'Next')]
    sleep   3

    #select occassion
    click element   xpath=//button[3]/div
    click element   xpath=//button[contains(.,'Next')]
    sleep   3

    #select Image
    click element   xpath=//button[5]
    click element   xpath=//button[contains(.,'Next')]
    sleep   3

    #Message
    click element   id=message
    input text      id=message      Thankyou
    input text      id=sender       MoniQA
    input text      xpath=//div[2]/div/div/div/input     MoniQA
    input text      xpath=//div[2]/div/div/div[2]/div/input     78 986 5321
    input text      xpath=//div[2]/div/div/input     joy@gmail.com
    reload page

 #Gift card Value
    click element   id=amount
    input text      id=amount   100
    set selenium implicit wait  5

    #select retailer
    scroll element into view    xpath=//button[12]
    click element       xpath=//button[12]
    scroll element into view        xpath=//button[contains(.,'Next')]
    click element   xpath=//button[contains(.,'Next')]
    sleep   3

    #select occassion
    click element   xpath=//button[3]/div
    click element   xpath=//button[contains(.,'Next')]
    sleep   3

    #select Image
    click element   xpath=//button[5]
    click element   xpath=//button[contains(.,'Next')]
    sleep   3

    #Message
    click element   id=message
    input text      id=message      Thankyou
    input text      id=sender       MoniQA
    input text      xpath=//div[2]/div/div/div/input     MoniQA
    input text      xpath=//div[2]/div/div/div[2]/div/input     78 986 5321
    input text      xpath=//div[2]/div/div/input     joy@gmail.com
    sleep       5
    click element   xpath=//button[contains(.,'Next')]
    #invoice
    scroll element into view    name=billing_email
    input text  name=billing_email      monicahn@yoyowallet.com
    input text  name=street_address     140 Capetown
    input text  name=city   capetown
    input text  name=post_code    20100
    input text  name=company    Nike
    input text  name=vat_number  444000
    click button   xpath=//button[contains(.,'Pay')]
    set browser implicit wait   8

    #payment
    click element   xpath=//div[@id='creditcard1']/div/input
    input text      xpath=//div[@id='creditcard1']/div/input    Moniqa test
    input text      xpath=//div[@id='creditcard2']/div/div/input     4000000000000002
    select from list by index     xpath=//div[@id='creditcard3']/div[2]/select    7
    input text      xpath=//div[@id='creditcard4']/div/input      123
    click button    nextBtn

    #submit
    wait until page contains    This alert does not appear when a LIVE PayGate ID is used   60
    handle alert    accept
    set selenium implicit wait  3
    click element   xpath=//tr[4]/td[2]/b/input
    set selenium implicit wait  10
    close browser









