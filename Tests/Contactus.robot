*** Settings ***
Library   SeleniumLibrary

*** Variables ***

${BASE_URL}     https://yoyogifts-qa.wigroup.co/
${browser}      Chrome


*** Test Cases ***
Contact us
    open browser    https://yoyogifts-qa.wigroup.co/    ${browser}
    maximize browser window
    reload page
    sleep   5
    click button    xpath=//button[contains(.,'Accept Cookies')]

    #select element
    scroll element into view   xpath=//a[contains(text(),'Contact us')]
    element should contain  xpath=//a[contains(text(),'Contact us')]   Contact us
    click element   xpath=//a[contains(text(),'Contact us')]
    set browser implicit wait   10

    #contact us
    input text  id=name     MoniQA
    input text  id=surname  Test
    input text  id=company  Nike
    input text  id=mobile   +27789223583246
    input text  id=email    moniqa@gmail.com
    input text  id=message  Well done
    click element   xpath=//button[@type='submit']
    sleep   5
    close browser